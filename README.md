# Exstāre

Verb

1. present active infinitive of [exstō](https://en.wiktionary.org/wiki/exsto#Latin)

## Running

Binary builds available from [itch.io](https://icrbow.itch.io/exstare)

### Benchmark mode

Start with `--timedemo 6000` to watch the world fly by for 6000 frames
with that pesky VSync limit removed.

## Issues

The game is running on ad-hoc OpenGL "engine" and I know very little of what I'm doing 🐶

If you encounter crashes, glitches and other kinds of game-breaking nasties, please add an issue.
Describe the OS and the hardware you're running the game on and
mark if you're willing to test hotfixes.

## Building

I build with [stack](https://haskellstack.org/) and don't know if `cabal-install` or `nix` works. Sorry. Feel free to contribute the bits necessary, though.

### Windows

Have GHC-8.6.5 installed:

    stack setup --stack-yaml stack-windows.yaml

Update msys2 and install SDL devel dependencies:

    stack exec -- pacman -Syu
    stack exec -- pacman -S mingw-w64-x86_64-pkg-config mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image

Build:

    stack --stack-yaml stack-windows.yaml build

Run:

    stack --stack-yaml stack-windows.yaml run

### Linux

Have GHC-8.8.3 installed:

    stack setup

Install SDL devel dependencies:

    apt install libsdl2-dev libsdl2-image-dev

Build Haskell deps and the game:

    stack build

Run:

    stack run

Run with args (example):

    stack run -- --timedemo 6000 +RTS -s

## Forking

Want a game of your own? Be my guest and fork the thing.
The major reason of why I'm in all this is I would like to see more games in Haskell.

* https://www.reddit.com/r/haskellgamedev/
* https://discordapp.com/invite/vxpWtBA


## Assets

Images are mostly placeholders right now, picked from free itch.io assets.

* Background: https://dinvstudio.itch.io/dynamic-space-background-lite-free
* Asteroids: https://wenrexa.itch.io/asteroidsn1
* Weapons are from that "7000 icons" Humble Bundle pack.
