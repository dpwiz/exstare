module Scene where

import Linear (V2(..))

import qualified Data.List.NonEmpty as NonEmpty

import Components

import qualified Apecs.System.Random as Random
import qualified Components.Comet.System as Comet
import qualified Components.It.System as It
import qualified Components.Platform.System as Platform

initialComets :: Int
initialComets = 1

init :: SystemW ()
init = do
  _it <- It.new

  (pos, vel) <- Random.pick $ NonEmpty.fromList
    [ ( Position $ V2 0 250
      , Velocity $ V2 50.0 0
      )
    , ( Position $ V2 0 250
      , Velocity $ V2 (-50.0) 0
      )
    ]
  _p1 <- Platform.new 50 pos vel

  sequence_ . replicate initialComets $
    Comet.spawnRandom True

  -- _c1 <- Comet.new Comet.Rock True 32
  --   ( Position $ V2 900 300
  --   , Velocity $ V2 (-5) (-5)
  --   )
  --   ( Turn 0
  --   , Spin 0.01
  --   )

  -- _c2 <- Comet.new Comet.Spiked True 64
  --   ( Position $ V2 300 900
  --   , Velocity $ V2 (-25) (-25)
  --   )
  --   ( Turn 0.5
  --   , Spin (-0.1)
  --   )

  pure ()
