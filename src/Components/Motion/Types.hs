module Components.Motion.Types where

import Apecs (Component(..), Map)

import qualified Lib.Trajectory as Trajectory

data Trajectory = Trajectory
  { _trajectorySegments :: ~[Trajectory.Segment]
  }

instance Show Trajectory where
  show =
    mappend "Trajectory " . show . take 1000 . _trajectorySegments

instance Component Trajectory where
  type Storage Trajectory = Map Trajectory
