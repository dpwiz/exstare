{-# LANGUAGE TemplateHaskell #-}

module Components.Explosion.Types where

import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)
import Linear (V2, V4)

data Explosion = Explosion
  { _explosionType  :: ExplosionType
  }
  deriving (Show)

data ExplosionType
  = Rings [Ring]
  deriving (Show)

data Ring = Ring
  { _ringTimer      :: Float
  , _ringTimeLimit  :: Float
  , _ringSpeed      :: Float
  , _ringSize       :: V2 Float
  , _ringInner      :: Float
  , _ringRadius     :: Float
  , _ringOuter      :: Float
  , _ringColor      :: V4 Float
  , _ringBackground :: Bool
  }
  deriving (Show)

makeLenses ''Explosion
makeLenses ''Ring

makeMapComponents
  [ ''Explosion
  ]
