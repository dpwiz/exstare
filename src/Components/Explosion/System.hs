module Components.Explosion.System
  ( spawnRings
  , someRings

  , destroy
  , ExplosionComponents

  , tick

  , module Components.Explosion.Types
  ) where

import Apecs (Entity, Not(..), cmap, newEntity, ($=))
import Control.Monad (guard)
import Linear (V4(..), (^*))

import Components (Position(..), Turn, SystemW)
import Components.Explosion.Types

type ExplosionComponents = (Explosion, Position, Turn)

spawnRings :: Position -> Turn -> [Ring] -> SystemW Entity
spawnRings pos turn rings =
  newEntity
    ( Explosion
        { _explosionType = Rings rings
        }
    , pos
    , turn
    )

someRings :: Float -> Float -> [Ring]
someRings size yield =
  [ Ring
      { _ringTimer      = 0
      , _ringTimeLimit  = 5
      , _ringSpeed      = 1.0
      , _ringSize       = 1.0 ^* size -- TODO: V2 1.6 0.9 ^* 128
      , _ringInner      = 0.33
      , _ringRadius     = 0.5
      , _ringOuter      = 0.33
      , _ringColor      = V4 1 (min 0.75 yield) 0 1
      , _ringBackground = True
      }
  , Ring
      { _ringTimer      = 0
      , _ringTimeLimit  = 2
      , _ringSpeed      = -1.0
      , _ringSize       = 1.0 ^* size
      , _ringInner      = 0
      , _ringRadius     = 0
      , _ringOuter      = 1
      , _ringColor      = V4 0.75 (min 0.75 yield) 0 1
      , _ringBackground = False
      }
  ]

destroy :: Entity -> SystemW ()
destroy entity = entity $= Not @ExplosionComponents

tick :: Float -> SystemW ()
tick dt = cmap $ \Explosion{..} ->
  case _explosionType of
    Rings rings ->
      let
        remaining = do
          ring@Ring{..} <- rings
          let timer' = _ringTimer + dt
          guard $ timer' < _ringTimeLimit
          pure ring
            { _ringTimer = timer'
            }
      in
        if null remaining then
          Nothing
        else
          Just $ Explosion
            { _explosionType = Rings remaining
            }
