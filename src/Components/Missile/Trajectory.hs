module Components.Missile.Trajectory where

import Linear (V2(..), normalize, (^*))

import Components

import qualified Lib.Trajectory as Trajectory

slugSpeed :: Float
slugSpeed = 120

projectSlug :: Position -> Velocity -> V2 Float -> [Trajectory.Segment]
projectSlug (Position pos) (Velocity vel) targetPos =
  Trajectory.project_ 1.0 (pos, vel + shootVel) force
  where
    shootVel = normalize (targetPos - pos) ^* slugSpeed
    force = Trajectory.Passive

rocketSpeed :: Float
rocketSpeed = 60

rocketThrust :: Float
rocketThrust = 30

rocketFuel :: Float
rocketFuel = 10

projectRocketShot :: Position -> Velocity -> V2 Float -> [Trajectory.Segment]
projectRocketShot (Position pos) (Velocity vel) targetPos =
  Trajectory.project_ 1.0 (pos, vel + shootVel) force
  where
    shootVel = normalize (targetPos - pos) ^* rocketSpeed
    force = Trajectory.Constant rocketThrust rocketFuel

projectRocketMotion :: Position -> Velocity -> Float -> [Trajectory.Segment]
projectRocketMotion (Position pos) (Velocity vel) rocketTimer =
  Trajectory.project_ 1.0 (pos, vel) force
  where
    force =
      if rocketTimer > rocketFuel then
        Trajectory.Passive
      else
        Trajectory.Constant rocketThrust (rocketFuel - rocketTimer)
