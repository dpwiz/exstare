module Components.Missile.System
  ( new
  , destroy
  , tick

  , shootSlug
  , shootRocket

  , module Components.Missile.Types
  ) where

import Apecs (Entity, Not(..), cmapM_, get, newEntity, ($=), ($~))
import Control.Lens ((.~))
import Control.Monad (void, when)
import GHC.Float (double2Float)
import Linear (V2(..), V3(..), angle, lerp, norm, normalize, unangle, (^*))
import Linear.Affine (distanceA)

import qualified Apecs.System.Random as Random
import qualified System.Random.MWC.Probability as Probability

import Components
import Components.Comet.Types (Comet(..))
import Components.It.Types (It)
import Components.Missile.Types
import Components.Missile.Trajectory (slugSpeed, rocketSpeed, rocketThrust, rocketFuel)

import qualified Components.Comet.System as Comet
import qualified Components.Debug.Tick as Debug
import qualified Components.Explosion.System as Explosion
import qualified Components.Motion.System as Motion
import qualified Components.Motion.Types as Motion
import qualified Lib
import qualified Lib.Trajectory as Trajectory

type MissileComponents = (Missile, Position, Mass, Velocity, Motion.Trajectory)

new :: MissileType -> Position -> Velocity -> Trajectory.Force -> SystemW Entity
new typ pos vel force = do
  trajectory <- Motion.projectForce pos vel force
  newEntity
    ( Missile
        { _missileTimer = 0
        , _missileFuse  = 20.0
        , _missileType  = typ
        }
    , pos
    , ( Mass 1
      , vel
      )
    , trajectory
    )

destroy :: Entity -> SystemW ()
destroy e = e $= Not @MissileComponents

tick :: Float -> SystemW ()
tick dt =
  cmapM_ $ \(Missile{..}, Position pos, Velocity vel, missile) -> do
    case _missileType of
      Slug ->
        pure ()
      Rocket -> do
        when (_missileTimer < rocketFuel) $ do
          let
            thrust = normalize vel ^* (rocketThrust * dt)
            turn = unangle vel / pi / 2

          missile $~ Velocity . (+ thrust) . unVelocity

          Lib.sometimes (30 * dt) $
            Debug.mark
              (Position pos) (Turn turn)
              2.0
              (V3 (norm vel / 200.0) 0.75 0.0)
              5

    let timer' = _missileTimer + dt
    if timer' >= _missileFuse then
      destroy missile
    else do
      missile $~ missileTimer .~ timer'

    withMissile missile $ \_m ->
      cmapM_ $ \(Orbital{..}, _it :: Maybe It, Position itPos) -> do
        let distance = distanceA pos itPos
        when (distance < _orbitalRadius * 0.9) $ do
          destroy missile

          void $ Explosion.spawnRings
            (Position pos)
            (Turn 0) -- TODO: unangle vel / pi / 2 + 0.5)
            (Explosion.someRings 10 $ norm vel / 200)

          -- TODO: register impact?

    withMissile missile $ \_m ->
      cmapM_ $ \(Comet{..}, Position cometPos, Mass cometMass, Velocity cometVelocity, comet) -> do
        let distance = distanceA pos cometPos
        when (distance < _cometSize * 0.66) $ do
          destroy missile
          Comet.destroy comet
          if _cometSize < 10 then do
            -- XXX: wiped out in a fiery explosion
            let yield = Comet.kineticEnergy cometMass cometVelocity -- TODO: extra missile yield
            void $ Explosion.spawnRings
              (Position $ lerp 0.5 pos cometPos)
              (Turn 0) -- TODO: unangle vel / pi / 2 + 0.5)
              (Explosion.someRings 15 yield)
          else do
            -- XXX: noisy fracture
            void $ Explosion.spawnRings
              (Position $ lerp 0.5 pos cometPos)
              (Turn 0) -- TODO: unangle vel / pi / 2 + 0.5)
              (Explosion.someRings (_cometSize * 0.5) 0)
            let
              newPiece fraction = void $ do
                jitter <- Random.sample $ do
                  rads <- double2Float <$> Probability.uniformR (0, pi)
                  impact <- double2Float <$> Probability.normal 0 5
                  pure $ angle rads ^* impact

                turn <- Random.range (0, 1)
                spin <- Random.sample $
                  double2Float <$> Probability.normal 0 (1/10)

                Comet.new
                  _cometType
                  False -- XXX: don't warp
                  (_cometSize * fraction)
                  (Position cometPos, Velocity $ cometVelocity + jitter)
                  (Turn turn, Spin spin)

            parts <- Random.sample $ do
              part <- double2Float <$> Probability.normal 0.5 0.1
              let clamped = max 0.1 $ min 0.9 part
              pure [clamped, 1 - clamped]

            mapM_ newPiece parts

withMissile :: Entity -> (Missile -> SystemW ()) -> SystemW ()
withMissile entity action =
  Apecs.get entity >>= \case
    Nothing ->
      pure ()
    Just m@Missile{} ->
      action m

shootSlug :: Position -> Velocity -> V2 Float -> SystemW Entity
shootSlug pos@(Position source) (Velocity sourceVel) target =
  new Slug pos vel Trajectory.Passive
  where
    launchVel = normalize (target - source)
    vel = Velocity $ sourceVel + launchVel ^* slugSpeed

shootRocket :: Position -> Velocity -> V2 Float -> SystemW Entity
shootRocket pos@(Position source) (Velocity sourceVel) target =
  new Rocket pos vel force
  where
    launchVel = normalize (target - source)
    vel = Velocity $ sourceVel + launchVel ^* rocketSpeed
    force = Trajectory.Constant rocketThrust rocketFuel
