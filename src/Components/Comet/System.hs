module Components.Comet.System
  ( spawnRandom

  , new
  , destroy
  , tick

  , kineticEnergy

  , module Components.Comet.Types
  ) where

import Apecs (Entity, Not(..), cmap, cmapM_, newEntity, ($=), ($~))
import Control.Lens ((.~), (+~))
import Control.Monad (void, when)
import GHC.Float (double2Float, float2Double)
import Linear (V2(..), angle, norm, normalize, quadrance, (^*))
import Linear.Affine (distanceA)

import qualified Apecs.System.Random as Random
import qualified System.Random.MWC.Probability as Probability

import Components
import Components.Comet.Types
import Components.It.Types

import qualified Config
import qualified Components.Explosion.System as Explosion
import qualified Components.Motion.System as Motion
import qualified Components.Motion.Types as Motion
import qualified Lib
import qualified Lib.Trajectory as Trajectory

type CometComponents =
  ( Comet
  , Mass
  , Position
  , Velocity
  , Turn, Spin
  , Motion.Trajectory
  )

new
  :: CometType
  -> Bool
  -> Float
  -> (Position, Velocity)
  -> (Turn, Spin)
  -> SystemW Entity
new typ warpIn size linear radial = do
  Lib.debugM $ "Comet sighted: " <> show (typ, linear)
  trajectory <- uncurry Motion.projectPassive linear
  newEntity $ comet trajectory
  where
    comet trajectory =
      ( Comet
          { _cometSighted = 0.0
          , _cometSize    = size
          , _cometTimer   = if warpIn then 0.0 else 2.0 -- TODO: use status field
          , _cometType    = typ
          }
      , Mass size
      , linear
      , radial
      , trajectory
      )

destroy :: Entity -> SystemW ()
destroy e = e $= Not @CometComponents

spawnRandom :: Bool -> SystemW (Maybe Entity)
spawnRandom warpIn = do
  typ <- Random.boundedEnum

  size <- Random.range (4, 128)

  position <- Random.sample $ do
    rads <- Probability.uniformR (-pi, pi)
    distance <- fmap double2Float $ Probability.normal
      (float2Double Config.cometSpawnMean)
      (float2Double Config.cometSpawnDev)
    pure $ angle rads ^* distance

  let towardsIt = normalize position ^* (-50.0)

  velocity <- Random.sample $ do
    rads <- Probability.uniformR (0, pi)
    speed <- double2Float <$> Probability.normal 0 10
    pure $ towardsIt + angle rads ^* speed

  let
    segments = Trajectory.project_ 1.0 (position, velocity) Trajectory.Passive
    Trajectory.Inspection{..} = Trajectory.inspect (take Config.cometSpawnProjection segments)
    periapsis = filter ((==) Trajectory.Periapsis . Trajectory.sAnn) apsides
  case (periapsis, entry) of
    ([], Nothing) ->
      -- XXX: not possible with a single body in general
      -- XXX: possible with limited projection time,
      --      which is "the comet doesn't appear on screen for too long"
      pure Nothing
    (Trajectory.Segment{sPos} : _rest, Nothing)
      | normalize sPos > 128.0 * 4 ->
          pure Nothing
    _ -> do
      turn <- Random.range (0, 1)
      spin <- Random.sample $
        double2Float <$> Probability.normal 0 (1/10)

      fmap Just $
        new typ warpIn size
          ( Position position
          , Velocity velocity
          )
          (Turn turn, Spin spin)

tick :: Float -> SystemW ()
tick dt = do
  Lib.sometimes (dt / 10.0) (spawnRandom True)

  cmap $ cometTimer +~ dt

  cmapM_ $ \(Comet{..}, Mass mass, Position pos, Velocity vel, comet) -> do
    cmapM_ $ \(Orbital{..}, Position orbitalPos, it') -> do
      let distance = distanceA pos orbitalPos

      when (distance < Config.cometSightRange) $
        comet $~ cometSighted .~ min 1.0 (_cometSighted + Config.cometSightSpeed * dt)

      -- TODO: do atmosphere burn and breakup

      when (distance < _orbitalRadius * 0.95) $ do
        let yield = kineticEnergy mass vel

        -- XXX: per type?
        let devastation = yield * Config.devastationScale

        Lib.debugM $ unlines $ map unwords
          [ ["Impact report:"]
          , ["  Mass:", show mass]
          , ["  Speed:", show (norm vel)]
          , ["  Energy:", show yield]
          , ["  Damage:"]
          , ["    Devastation:", show devastation]
          ]

        void $ Explosion.spawnRings
          (Position pos)
          (Turn 0) -- TODO: unangle vel / pi / 2 + 0.5)
          (Explosion.someRings (max 64 _cometSize) yield)

        destroy comet
        case it' of
          Nothing ->
            pure ()
          Just (It{}, it) ->
            it $~ itDevastation +~ devastation

    when (norm pos > Config.cometSpawnMean * 3) $ do
      Lib.debugM $ unwords
        [ "Comet gone:"
        , show pos
        , show vel
        ]
      destroy comet

kineticEnergy :: Float -> V2 Float -> Float
kineticEnergy mass velocity = 0.5 * normMass * normVel2
  where
    normMass = mass * Config.yieldScaleMass
    normVel2 = quadrance velocity * (Config.yieldScaleSpeed ** 2)
