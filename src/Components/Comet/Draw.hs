module Components.Comet.Draw where

import Control.Lens ((&), (.~))
import Control.Monad (when)
import Linear (V2(..), V4(..), (^*))
import System.FilePath ((</>))

import qualified Apecs

import Components
import Components.Comet.Types (Comet(..), CometType(..))
import Lib.Trajectory.Draw (drawSegments)

import qualified Components.Motion.Types as Motion
import qualified Components.Textures as Textures
import qualified Config
import qualified Lib.Window as Window
import qualified Programs.Sprite as Sprite


draw :: SystemW ()
draw = do
  onCamera <- Window.onCamera Config.cometMassMax
  Apecs.cmapM_ $ \(Comet{..}, Mass mass, Position pos, Turn turn, comet) -> do
    let
      texture = case _cometType of
        Rock   -> Textures.Key $ "comet" </> "rock"
        Spiked -> Textures.Key $ "comet" </> "spiked"

      warpColor = V4 1 0.25 2 2
      eff =
        if _cometTimer < 2.0 then
          -- XXX: warp-in
          mempty
            & Sprite.effectsTurn    .~ turn
            & Sprite.effectsOpacity .~ min 1.0 (_cometTimer / 1.5)
            & Sprite.effectsOutline .~
                if _cometTimer < 1.5 then
                  Just (warpColor ^* (_cometTimer / 1.5))
                else
                  Just (warpColor ^* (1 - (_cometTimer - 1.5) * 2))
        else
          mempty
            -- & Sprite.effectsGamma .~ V3 2 1 1 -- TODO: atmosphere burn
            & Sprite.effectsTurn .~ turn
            & Sprite.effectsOutline .~
                if _cometSighted == 0 then
                  Nothing
                else
                  Just (V4 1 0.25 0.125 $ _cometSighted * 1.333)

    let limit = truncate $ Config.cometSightProjection * _cometSighted
    when (limit > 0) $
      Apecs.get comet >>= \case
        Motion.Trajectory{..} ->
          drawSegments limit (V4 1 0 0 0.5) _trajectorySegments

    let V2 px py = pos
    when (onCamera px py) $ do
      Sprite.draw eff texture (1.0 ^* mass) pos
