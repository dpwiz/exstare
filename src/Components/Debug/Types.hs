{-# LANGUAGE TemplateHaskell #-}

module Components.Debug.Types where

import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)
import Linear (V3)

data Foo = Foo
  { _fooTimer :: Float
  , _fooColor :: V3 Float
  , _fooSize  :: Float
  }
  deriving (Show)

makeLenses ''Foo

makeMapComponents
  [ ''Foo
  ]
