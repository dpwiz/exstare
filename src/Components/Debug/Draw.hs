module Components.Debug.Draw where

import Control.Monad (when)
import Linear (V2(..), V3(..))

import qualified Apecs
import qualified Graphics.Rendering.OpenGL as GL

import Components
import Components.Debug.Types (Foo(..))

import qualified Programs.Solid as Solid
import qualified Lib
import qualified Lib.Window as Window

draw :: SystemW ()
draw = do
  onCamera <- Window.onCamera 32
  Apecs.cmapM_ $ \(Foo{..}, Position (V2 px py), Turn turn) ->
    when (onCamera px py) $ do
      let V3 r g b = _fooColor
      let a = sqrt (_fooTimer / 60.0)
      Solid.drawWith (GL.Vector4 r g b a) $ \coord2d ->
        Lib.drawQuads coord2d $
          Lib.quadTurn _fooSize _fooSize px py turn
