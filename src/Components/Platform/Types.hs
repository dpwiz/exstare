{-# LANGUAGE TemplateHaskell #-}

module Components.Platform.Types where

import Apecs.TH (makeMapComponents)
import Control.Lens.TH (makeLenses)

import Components.Missile.Types (MissileType)

data Platform = Platform
  { _platformSize   :: Float
  , _platformTimer  :: Float
  , _platformWeapon :: MissileType
  } deriving (Show)

makeLenses ''Platform

makeMapComponents
  [ ''Platform
  ]
