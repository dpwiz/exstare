module MainLoop where

import Control.Monad (when)
import Control.Monad.IO.Class (MonadIO(..))
import Data.StateVar (($=))
import Linear (V4(..), (^*))
import System.Mem (performGC)

import qualified Apecs
import qualified Graphics.Rendering.OpenGL as GL
import qualified SDL

import Components (Camera(..), Window(..), SystemW)

import qualified Config
import qualified Draw
import qualified Events
import qualified Lib
import qualified Lib.Window as Window
import qualified Tick

import Text.Printf (printf)

go :: SDL.Window -> Int -> Float -> SystemW Int
go sdlWindow framesLeft prevSeconds = do
  curSeconds <- Lib.seconds

  (camera, window) <- Apecs.get Apecs.global

  quit <- Events.handle window camera
  if quit || framesLeft == 0 then
    pure $ negate framesLeft
  else do
    let target = Config.dt
    Tick.tick target
    -- TODO: wait for appropriate time wrt. FPS
    Lib.sometimes target $ do
      let
        actual = curSeconds - prevSeconds
        budget = target - actual
      Lib.debugM $
        printf "Timing: t=%.2f a=%.2f b=%.2f"
          (target * 1000)
          (actual * 1000)
          (budget * 1000)

    liftIO $ do
      -- XXX: move to size handler?
      GL.viewport $=
        ( GL.Position 0 0
        , GL.Size
            (truncate $ _windowWidth window)
            (truncate $ _windowHeight window)
        )

      GL.clearColor $= GL.Color4 0 0 0 1
      GL.clear [GL.ColorBuffer] -- XXX: depth, etc.?

    let windowQuad = fmap Lib.glDouble (Window.quad window)

    liftIO $ do
      GL.loadIdentity
      let V4 xStart xEnd yStart yEnd = windowQuad
      GL.ortho xStart xEnd yStart yEnd 0 (-100)
    Draw.drawBackdrop

    liftIO $ do
      GL.loadIdentity
      let V4 xStart xEnd yStart yEnd = windowQuad ^* (Lib.glDouble $ _cameraScale camera)
      GL.ortho xStart xEnd yStart yEnd 0 (-100)
    Draw.drawScene

    liftIO $ do
      GL.loadIdentity
      let V4 xStart xEnd yStart yEnd = windowQuad
      GL.ortho xStart xEnd yStart yEnd 0 (-100)
    Draw.drawUI

    SDL.glSwapWindow sdlWindow

    when (framesLeft < 0) $
      liftIO performGC

    go sdlWindow (framesLeft - 1) curSeconds
