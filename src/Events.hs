module Events where

import Apecs (get, global, liftIO)
import Data.Foldable (for_)
import Linear (V2(..))
import System.Exit (exitSuccess)

import qualified Apecs
import qualified SDL

import Components

import qualified Components.Platform.System as Platform
import qualified Components.Time.System as Time
import qualified Lib
import qualified Lib.Window as Window

onKeyDown :: SDL.Keysym -> SystemW ()
onKeyDown SDL.Keysym{..} = do
  case keysymKeycode of
    SDL.KeycodeEscape -> do
      SDL.quit
      liftIO exitSuccess
    SDL.KeycodePause ->
      Time.togglePause
    SDL.KeycodeP ->
      Time.togglePause
    SDL.KeycodeBackquote ->
      Platform.nextWeapon
    SDL.Keycode1 ->
      Platform.selectWeapon 1
    SDL.Keycode2 ->
      Platform.selectWeapon 2
    _ ->
      pure ()

onKeyUp :: SDL.Keysym -> SystemW ()
onKeyUp SDL.Keysym{..} =
  pure ()

onCursorMove :: Cursor -> SystemW ()
onCursorMove _cur =
  pure ()

onMouseDown :: SDL.MouseButton -> SystemW ()
onMouseDown = \case
  SDL.ButtonLeft ->
    Platform.shoot
  SDL.ButtonRight ->
    Platform.nextWeapon
  _ ->
    pure ()

onMouseUp :: SDL.MouseButton -> SystemW ()
onMouseUp = \case
  SDL.ButtonLeft ->
    -- XXX: consider DragStart
    pure ()
  SDL.ButtonRight ->
    Platform.altShoot
  _ ->
    pure ()

onZoom :: Bool -> SystemW ()
onZoom up = do
  Camera{..} <- get global
  let
    newScale =
      max 0.5 . min 2.0 $
        if up then
          _cameraScale * 0.9
        else
          _cameraScale * 1.1
    newCamera = Camera
      { _cameraScale = newScale
      , ..
      }

  Apecs.set global newCamera
  Apecs.cmap $ \Cursor{..} ->
    Cursor
      { _cursorCamera = Window.windowToCamera newCamera _cursorWindow
      , ..
      }

-- | Extract and convert SDL event data
handle :: Window -> Camera -> SystemW Bool
handle window camera = do
  events <- SDL.pollEvents

  for_ events $ \e -> case SDL.eventPayload e of
    SDL.KeyboardEvent eventData ->
      onKeyboard eventData

    SDL.MouseButtonEvent eventData ->
      onButton eventData

    SDL.MouseMotionEvent eventData ->
      onMotion eventData

    SDL.MouseWheelEvent eventData ->
      onMouseWheelEvent eventData

    SDL.WindowShownEvent eventData ->
      onWindowShown eventData

    SDL.WindowSizeChangedEvent eventData ->
      onWindowSizeChanged eventData

    _ ->
      pure ()

  pure $ SDL.QuitEvent `elem` map SDL.eventPayload events

  where
    posToWindow (SDL.P pos) =
      Window.screenToWindow window pos

    onKeyboard SDL.KeyboardEventData{..} =
      case keyboardEventKeyMotion of
        SDL.Pressed ->
          onKeyDown keyboardEventKeysym
        SDL.Released ->
          onKeyUp keyboardEventKeysym

    onButton SDL.MouseButtonEventData{..} =
      case mouseButtonEventMotion of
        SDL.Pressed ->
          onMouseDown mouseButtonEventButton

        SDL.Released ->
          onMouseUp mouseButtonEventButton

    onMotion SDL.MouseMotionEventData{..} = do
      let
        win = posToWindow mouseMotionEventPos
        cur = Components.Cursor
          { _cursorWindow = win
          , _cursorCamera = Window.windowToCamera camera win
          }
      Apecs.set Apecs.global cur
      onCursorMove cur

    onMouseWheelEvent SDL.MouseWheelEventData{..} = do
      let
        V2 _x y = mouseWheelEventPos
        up = (y > 0) /= (mouseWheelEventDirection == SDL.ScrollFlipped)
      Lib.debugM $ "Mouse wheel: " <> show (mouseWheelEventDirection, mouseWheelEventPos, up)
      onZoom up

    onWindowShown SDL.WindowShownEventData{..} = do
      size <- SDL.glGetDrawableSize windowShownEventWindow
      Lib.debugM $ "Window shown: " <> show size
      Window.setSize size

    onWindowSizeChanged SDL.WindowSizeChangedEventData{..} = do
      Lib.debugM $ "Window resized: " <> show windowSizeChangedEventSize
      let V2 width height = fmap fromIntegral windowSizeChangedEventSize
      Apecs.set Apecs.global $ Components.Window
        { _windowWidth  = width
        , _windowHeight = height
        , _windowScale  = 1.0
        }
