module Tick where

import Components (SystemW)

import qualified Components.Comet.System as Comet
import qualified Components.Debug.Tick as Debug
import qualified Components.Explosion.System as Explosion
import qualified Components.It.System as It
import qualified Components.Missile.System as Missile
import qualified Components.Motion.System as Motion
import qualified Components.Platform.System as Platform
import qualified Components.Time.System as Time

tick :: Float -> SystemW ()
tick dt = do
  -- Lib.debugM $ show dt

  Time.unlessPaused $ do
    Motion.tick dt

    Comet.tick dt
    Debug.tick dt
    Explosion.tick dt
    It.tick dt
    Missile.tick dt
    Platform.tick dt
