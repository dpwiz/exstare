module Programs.Ring where

import Control.Monad.IO.Class (MonadIO)
import GHC.Stack (HasCallStack)
import Linear (V2(..))

import qualified Apecs
import qualified Graphics.Rendering.OpenGL as GL

import Lib (ToGL(..))

import qualified Components.Programs as Programs
import qualified Lib

draw
  :: ( Apecs.Has w m Programs.Programs
     , HasCallStack
     , MonadIO m
     , Lib.ToGL color
     , GLType color ~ GL.Vector4 Float
     )
  => V2 Float
  -> V2 Float
  -> Float
  -> (Float, Float, Float)
  -> color
  -> Apecs.SystemT w m ()
draw quadSize offset turn (inner, radius, outer) color {- seconds screenSize -} =
  Programs.withCompiled (Programs.Key "ring") $ \setUniform withAttribute -> do
    -- setUniform "u_time" seconds
    -- setUniform "u_resolution" $ toGL screenSize
    setUniform "u_size" $ toGL quadSize -- XXX: For centering polars coordinates
    setUniform "u_radius" radius
    setUniform "u_inner" inner
    setUniform "u_outer" outer
    setUniform "u_color" $ Lib.toGL color

    -- setUniform "u_offset" $ toGL offset

    withAttribute "texcoord" $ \texcoord ->
      Lib.withVertexAttribArray texcoord Lib.texVertices $
        withAttribute "coord2d" $ \coord2d ->
          Lib.drawQuads coord2d $
            Lib.quadTurn width height right top turn
  where
    V2 width height = quadSize
    V2 right top = offset
