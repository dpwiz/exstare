module Programs.Textured where

import Data.StateVar (($=))

import qualified Graphics.Rendering.OpenGL as GL

import Components (SystemW)

import qualified Components.Programs as Programs
import qualified Components.Textures as Textures
import qualified Lib

drawWith
  :: Textures.Key
  -> (GL.AttribLocation -> SystemW ())
  -> SystemW ()
drawWith key action =
  Programs.withCompiled (Programs.Key "texture") $ \setUniform withAttribute -> do
    Textures.Texture{textureObject} <- Textures.get key

    GL.activeTexture $= GL.TextureUnit 0
    GL.textureBinding GL.Texture2D $= Just textureObject
    setUniform "texture" $ GL.TextureUnit 0
    setUniform "u_invGamma" $ GL.Vector4 @Float 1 1 1 1
    setUniform "u_opacity" (1.0 :: Float)

    withAttribute "texcoord" $ \texcoord ->
      Lib.withVertexAttribArray texcoord Lib.texVertices $
        withAttribute "coord2d" action
