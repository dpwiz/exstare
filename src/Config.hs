module Config where

-- screenWidth :: Float
-- screenWidth = 1600

-- screenHeight :: Float
-- screenHeight = 900

dt :: Float
dt = 1/60

cometSightRange :: Float
cometSightRange = 1000

-- | Time to fully resolve comet params
cometSightSpeed :: Float
cometSightSpeed = recip 30

cometSightProjection :: Float
cometSightProjection = 100

cometSpawnMean :: Float
cometSpawnMean = cometSightRange * 1.5

cometSpawnDev :: Float
cometSpawnDev = cometSpawnMean * 0.25

cometSpawnProjection :: Int
cometSpawnProjection = truncate $ 60 / dt

itRadius :: Float
itRadius = 128

itMass :: Float
itMass = 10000

cometMassMin :: Float
cometMassMin = 8

cometMassMax :: Float
cometMassMax = 128

cometMassMean :: Float
cometMassMean = (cometMassMin + cometMassMax) / 2

yieldScaleMass :: Float
yieldScaleMass = recip cometMassMean

-- XXX: typical hit speed
yieldScaleSpeed :: Float
yieldScaleSpeed = recip 100

devastationScale :: Float
devastationScale = 0.5

-- XXX: one grade of devastation per minute
devastationDecay :: Float
devastationDecay = 1/60
